from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from django.urls import reverse_lazy
from todos.models import TodoItem, TodoList


# Create your views here.
# views for TodoList
class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    fields = ["name"]
    template_name = "todos/new.html"
    # success_url = reverse_lazy('show_todolist', )

    def get_success_url(self) -> str:
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    fields = ["name"]
    template_name = "todos/edit.html"

    def get_success_url(self) -> str:
        return reverse_lazy("show_todolist", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("list_todos")


# views for TodoItem
class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/item_new.html"
    fields = "__all__"

    def get_success_url(self) -> str:
        return reverse_lazy("show_todolist", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/item_edit.html"
    fields = "__all__"

    def get_success_url(self) -> str:
        return reverse_lazy("show_todolist", args=[self.object.list.id])
